import logo from "./logo.svg";
import "./App.css";
import React, { Component } from "react";

class Text extends Component {
  render() {
    return (
      <span
        style={{
          color: this.props.color,
        }}
      >
        {this.props.value}
      </span>
    );
  }
}

class NewText extends Component {
  render() {
    return (
      <span
        style={{
          color: this.props.color,
          backgroundColor: this.props.bgColor,
          fontSize: this.props.font,
          border: this.props.border,
          fontWeight: this.props.weight,
        }}
      >
        ate onde posso ir???
      </span>
    );
  }
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>
          Hello, <Text color="green" value="Kenzie" />
        </h1>
        <h2>
          Estou testando:{" "}
          <NewText
            color="black"
            bgColor="blue"
            font="50px"
            border="solid 2px purple"
            weight="bold"
          />
        </h2>
      </header>
    </div>
  );
}

export default App;
